defmodule Servy.PledgeServer do
  # GenServer starter options, working with Supervisor
  use GenServer, restart: :temporary

  @process_name __MODULE__

  defmodule State do
    defstruct cache_size: 3, pledges: []
  end

  # Client Interface Functions
  def start_link(_args) do
    IO.puts("\nStarting the pledge server...")

    GenServer.start_link(__MODULE__, %State{}, name: @process_name)
  end

  def create_pledge(name, amount),
    do: GenServer.call(@process_name, {:create_pledge, name, amount})

  def recent_pledges,
    do: GenServer.call(@process_name, :recent_pledges)

  def total_pledged,
    do: GenServer.call(@process_name, :total_pledged)

  def clear do
    send(@process_name, :clear)
  end

  def set_cache_size(size) do
    GenServer.cast(@process_name, {:set_cache_size, size})
  end

  # Server Callbacks

  def init(state) do
    recent_pledges = fetch_recent_pledges_from_service()

    new_state = %{state | pledges: recent_pledges}

    {:ok, new_state}
  end

  def handle_call({:set_cache_size, size}, state) do
    new_state = %{state | cache_size: size}

    {:noreply, new_state}
  end

  def handle_cast(:clear, _from, state),
    do: {:noreply, %{state | pledges: []}}

  def handle_call({:create_pledge, name, amount}, _from, state) do
    {:ok, id} = send_pledge_to_service(name, amount)

    most_recent_pledges = Enum.take(state.pledges, state.cache_size - 1)
    cached_pledges = [{name, amount} | most_recent_pledges]

    new_state = %{state | pledges: cached_pledges}

    {:reply, id, new_state}
  end

  def handle_call(:recent_pledges, _from, state),
    do: {:reply, state.pledges, state}

  def handle_call(:total_pledged, _from, state) do
    total = Enum.map(state.pledges(), &elem(&1, 1)) |> Enum.sum()

    {:reply, total, state}
  end

  def handle_info(message, state) do
    IO.puts("Can't touch this! #{inspect(message)}")

    {:noreply, state}
  end

  defp send_pledge_to_service(_name, _amount),
    do: {:ok, "pledge-#{:rand.uniform(1000)}"}

  defp fetch_recent_pledges_from_service do
    # CODE GOES HERE TO FETCH RECENT PLEDGES FROM EXTERNAL SERVICE

    # Example return value:
    [{"wilma", 15}, {"fred", 25}]
  end
end
