defmodule Servy.Plugins do
  alias Servy.Conv

  @no_found_code 404

  # Makes the struct looks like a type safety
  # Turning explicit the Conv "type" on Patter Matching
  def rewrite_path(%Conv{path: "/wildlife"} = conv) do
    %{conv | path: "/wildthings"}
  end

  def rewrite_path(conv), do: conv

  def track(%Conv{status: @no_found_code, path: path} = conv) do
    if Mix.env() != :test do
      IO.puts("Warning! #{path} is on the loose!")
    end

    conv
  end

  def track(%Conv{} = conv), do: conv

  def log(conv) do
    if Mix.env() == :dev do
      IO.inspect(conv)
    end

    conv
  end
end
