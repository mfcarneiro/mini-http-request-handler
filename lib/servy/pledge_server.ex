defmodule Servy.GenericServer do
  def start(callback_module, initial_state, process_name) do
    pid = spawn(__MODULE__, :listen_loop, [initial_state, callback_module])
    Process.register(pid, process_name)
  end

  def call(pid, message) do
    send(pid, {:call, self(), message})

    receive do
      {:response, response} -> response
    end
  end

  def cast(pid, message) do
    send(pid, {:cast, message})
  end

  def listen_loop(state, callback_module) do
    receive do
      {:call, sender, message} when is_pid(sender) ->
        {response, new_state} = callback_module.handle_call(message, state)
        send(sender, {:response, response})
        listen_loop(new_state, callback_module)

      {:cast, message} ->
        new_state = callback_module.handle_cast(message, state)
        listen_loop(new_state, callback_module)

      unexpected ->
        IO.puts("Unexpected messages #{unexpected}")

        listen_loop(state, callback_module)
    end
  end
end

defmodule Servy.PledgeServerOld do
  alias Servy.GenericServer

  @process_name __MODULE__

  # Client Interface Functions
  def start(initial_state \\ []) do
    IO.puts("\nStarting the pledge server...")

    GenericServer.start(__MODULE__, initial_state, @process_name)
  end

  def create_pledge(name, amount),
    do: GenericServer.call(@process_name, {:create_pledge, name, amount})

  def recent_pledges,
    do: GenericServer.call(@process_name, :recent_pledges)

  def total_pledged,
    do: GenericServer.call(@process_name, :total_pledged)

  def clear do
    send(@process_name, :clear)
  end

  # Server Callbacks

  def handle_cast(:clear, _state),
    do: []

  def handle_call({:create_pledge, name, amount}, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    most_recently_pledges = Enum.take(state, 2)
    new_state = [{name, amount} | most_recently_pledges]

    {id, new_state}
  end

  def handle_call(:recent_pledges, state),
    do: {state, state}

  def handle_call(:total_pledged, state) do
    total = Enum.map(state, &elem(&1, 1)) |> Enum.sum()

    {total, state}
  end

  defp send_pledge_to_service(_name, _amount),
    do: {:ok, "pledge-#{:rand.uniform(1000)}"}
end
