defmodule Servy.BearController do
  @ok_code 200
  @create_code 201
  @templates_path Path.expand("../templates", File.cwd!())

  @moduledoc "
    Check notes from 16 - Comprehensions class
    Using templates with eex functions to precompiling them
  "
  alias Servy.Wildthings
  alias Servy.Bear

  defp render(conv, template, bindings) do
    content =
      @templates_path
      |> Path.join(template)
      |> EEx.eval_file(bindings)

    %{conv | status: @ok_code, resp_body: content}
  end

  def index(conv) do
    # Refactoring!
    # items =
    #   Wildthings.list_bears()
    #   |> Enum.filter(fn bear -> bear.type == "Grizzly" end)
    #   |> Enum.sort(fn bear1, bear2 -> bear1.name <= bear2.name end)
    #   |> Enum.map(fn bear ->  end)
    #   |> Enum.join()

    bears =
      Wildthings.list_bears()
      |> Enum.sort(&Bear.order_by_asc_name(&1, &2))

    render(conv, "index.eex", bears: bears)
  end

  def show(conv, %{"id" => id}) do
    bear = Wildthings.get_bear(id)

    render(conv, "show.eex", bear: bear)
  end

  def create(conv, %{"name" => name, "type" => type} = _params) do
    %{
      conv
      | status: @create_code,
        resp_body: "Created a #{type} bear named #{name}!"
    }
  end
end
