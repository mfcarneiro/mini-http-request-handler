defmodule Servy.Handler do
  # import Servy.Plugins -> import all

  # More selective approach, with the arities
  import Servy.Plugins, only: [rewrite_path: 1, track: 1, log: 1]
  import Servy.Parser, only: [parse: 1]

  alias Servy.Conv
  alias Servy.BearController
  alias Servy.Api.BearController, as: BearApi
  alias Servy.VideoCam

  @ok_code 200
  @no_found_code 404
  @forbidden_code 403
  @error_code 500

  def handle(request) do
    # conv = parse(request)
    # conv = route(conv)
    # format_response(conv)

    request
    |> parse
    |> rewrite_path
    |> log
    |> route
    |> track
    |> format_response
  end

  # def route(conv) do
  #   # --> Same as Map.put()
  #   # --> This way only updates values that exists inside map
  #   # %{conv | resp_body: "Bears, Lions and Tigers"}
  # end

  # Function clauses

  # -> Case clauses!
  # def route(%{method: "GET", path: "/about"} = conv) do
  #   # Using Case statement, the order matters!
  #   case File.read("../pages/about.html") do
  #     {:ok, content} -> %{conv | status: @ok_code, resp_body: content}
  #     {:error, :enoent} -> %{conv | status: @no_found_code, resp_body: "File not found!"}
  #     {:error, reason} -> %{conv | status: @error_code, resp_body: "File error: #{reason}"}
  #   end
  # end
  #

  # -> Function clauses!
  def route(%Conv{method: "POST", path: "/pledges"} = conv) do
    Servy.PledgeController.create(conv, conv.params)
  end

  def route(%Conv{method: "GET", path: "/pledges"} = conv) do
    Servy.PledgeController.index(conv)
  end

  def route(%Conv{method: "GET", path: "/sensors"} = conv) do
    sensor_data = Servy.SensorServer.get_sensor_data()

    %{conv | status: 200, resp_body: inspect(sensor_data)}
  end

  def route(%Conv{method: "GET", path: "/wildthings"} = conv) do
    BearController.index(conv)
  end

  def route(%Conv{method: "GET", path: "/bears"} = conv) do
    BearController.index(conv)
  end

  def route(%Conv{method: "GET", path: "/api/bears"} = conv) do
    BearApi.index(conv)
  end

  def route(%Conv{method: "GET", path: "/bears/" <> id} = conv) do
    params = Map.put(conv.params, "id", id)

    BearController.show(conv, params)
  end

  def route(%Conv{method: "POST", path: "/bears"} = conv) do
    BearController.create(conv, conv.params)
  end

  def route(%Conv{method: "GET", path: "/hibernate/" <> time} = conv) do
    time |> String.to_integer() |> :timer.sleep()

    %{conv | status: 200, resp_body: "Awake!"}
  end

  def route(%Conv{method: "GET", path: "/kaboom"} = _conv) do
    raise "Kaboom!"
  end

  def route(%Conv{method: "GET", path: "/about"} = conv),
    do:
      pages_absolute_path("../pages", "about")
      |> handle_file(conv)

  def route(%Conv{method: "DELETE", path: "/bears/" <> id} = conv) do
    %{conv | status: @forbidden_code, resp_body: "You can't delete the bear #{id}!"}
  end

  # Catch (Default) route needs to be in last because Elixir matches top to bottom
  def route(%Conv{path: path} = conv) do
    %{conv | status: @no_found_code, resp_body: "No #{path} here!"}
  end

  #

  def format_response(%Conv{} = conv) do
    """
    HTTP/1.1 #{Conv.full_status(conv)}\r
    Content-Type: #{conv.resp_content_type}\r
    Content-Length: #{byte_size(conv.resp_body)}\r
    \r
    #{conv.resp_body}
    """
  end

  def pages_absolute_path(folder_name, file_name),
    # (File.cwd!) -> mix always runs from the root project directory which is the top-level project directory
    # File.cwd always returns the top-level servy directory
    # The "!" convention will always raise an exception if it something fails
    do:
      Path.expand(folder_name, File.cwd!())
      |> Path.join(file_name <> ".html")
      |> File.read()

  # -> Function clauses!
  def handle_file({:ok, content}, conv),
    do: %{conv | status: @ok_code, resp_body: content}

  def handle_file({:error, :enoent}, conv),
    do: %{conv | status: @no_found_code, resp_body: "File not found!"}

  def handle_file({:error, reason}, conv),
    do: %{conv | status: @error_code, resp_body: "File error: #{reason}"}
end

# Class 5 - Immutable data (Notes)
#  - Using byte_size instead String.length on utf-8 encode
#  Class 8 - Notes - Regex examples
