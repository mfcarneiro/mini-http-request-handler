defmodule Servy.KickStarter do
  use GenServer

  def start_link(_args) do
    IO.puts("Starting the KickServer...")

    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Process.flag(:trap_exit, true)

    server_pid = start_server()

    {:ok, server_pid}
  end

  def handle_info({:EXIT, _pid, reason}, _state) do
    IO.puts("HttpServer exited (#{inspect(reason)})")

    server_pid = start_server()

    {:noreply, server_pid}
  end

  defp start_server do
    IO.puts("Starting the HTTP Server...")

    # Same as Process.link, but both runs in the same pid
    port = Application.get_env(:servy, :port)
    server_pid = spawn_link(Servy.HttpServer, :start, port)

    Process.register(server_pid, :http_server)
    # Process.link(server_pid)

    server_pid
  end
end
